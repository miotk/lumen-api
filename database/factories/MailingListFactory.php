<?php

/**
 * --------------------------------------------------------------------------
 * Model Factories
 * --------------------------------------------------------------------------
 * Here you may define all of your model factories. Model factories give
 * you a convenient way to create models for testing and seeding your
 * database. Just tell the factory how a default model should look.
 *
 * @var  \Illuminate\Database\Eloquent\Factory $factory
 */

use App\Models;

$factory->define(Models\MailingList::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => 'connor.miotk1996@icloud.com',
    ];
});
