<?php

use App\Models\Post;
use App\Models\PostComment;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = factory(User::class, 15)->create();

        $users->each(function (User $user) {
            $posts = $user->posts()->saveMany(factory(Post::class, rand(3, 7))->make());

            $posts->each(function (Post $post) {
                $post->comments()->saveMany(factory(PostComment::class, rand(1, 15))->make());
            });
        });
    }
}
