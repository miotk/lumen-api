<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    protected $tablesToTruncate = [
        'users',
        'posts',
        'post_comments',
        'mailing_lists',
        'mailing_list_history',
        'post_images',
        'settings',
        'user_avatars',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->tablesToTruncate as $truncateTable) {
            DB::table($truncateTable)->truncate();
        }

        $this->call('UsersTableSeeder');
    }
}
