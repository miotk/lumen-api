<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticationController extends Controller
{
    /**
     * Log the user in
     *
     * @param \Illuminate\Http\Request  $request
     * @throws \Illuminate\Validation\ValidationException
     * @return array|string
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::query()->where('username', $request->input('username'))->firstOrFail();

        if (!password_verify($request->input('password'), $user->password)) {
           return response()->json([
               'message' => 'Invalid username or password'
           ], 400);
        }

        return [
            'token' => JWTAuth::fromUser($user, [])
        ];
    }

    public function logout(Request $request)
    {
        //
    }

    /**
     * Register a new user
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'email' => 'required|unique:users,email',
        ]);

        return User::query()->create([
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'email' => $request->input('email')
        ]);
    }
}
