<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Create a new post
     *
     * @param \App\Models\User         $user
     * @param \Illuminate\Http\Request $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function create(User $user, Request $request)
    {
        $this->validate($request, [
           'name' => 'required|max:50',
           'slug' => 'required|unique:posts,slug|max:50',
           'excerpt' => 'max:250',
           'content' => 'required'
        ]);

        return Post::query()->create([
            'user_id' => $user->id,
            'name' => $request->input('name'),
            'slug' => $request->input('slug') ?? str_slug($request->name),
            'excerpt' => $request->input('excerpt') ?? $this->excerpt($request->input('content'), 250),
            'content' => $request->input('content')
        ]);
    }

    /**
     * Find a post by the slug
     *
     * @param                          $slug
     * @return array
     */
    public function find($slug)
    {
        $comments = [];
        $post = Post::query()->where('slug', $slug)->with(['author', 'comments'])->firstOrFail();

        $post->comments->each(function ($comment) use (&$comments) {
           $comments[] = [
               'user' => $comment->name,
               'comment' => $comment->content,
               'date' => $comment->created_at->format('d/m/Y')
           ];
        });

        return [
            'title' => $post->name,
            'content' => $post->content,
            'created_at' => $post->created_at->format('d/m/Y'),
            'author' => [
                'name' => $post->author->username
            ],
            'comments' => $comments
        ];
    }

    /**
     * Delete a post
     *
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete($id)
    {
        $post = Post::query()->findOrFail($id)->softDelete();

        if ($post) {
            return response()->json([
                'message' => 'Post successfully deleted'
            ], 200);
        }

        return response()->json([
            'message' => 'Unable to delete post'
        ], 400);
    }

    /**
     * Update an existing post
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'slug' => 'unique:posts,slug,' . $id
        ]);

        $post = Post::query()->findOrFail($id)->update($request->all());

        if ($post) {
            return Post::query()->where('id', $id)->first();
        }

        return response()->json([
            'message' => 'Unable to update post'
        ], 400);
    }

    /**
     * Create an excerpt
     *
     * @param $title
     * @param $cutOffLength
     * @return string
     */
    protected function excerpt($title, $cutOffLength) {
        $titleLength = strlen($title);

        do {
            $cutOffLength++;
            $charAtPosition = substr($title, $cutOffLength, 1);
        } while ($cutOffLength < $titleLength && $charAtPosition != " ");

        return substr($title, 0, $cutOffLength) . '...';

    }
}
