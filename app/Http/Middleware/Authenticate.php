<?php

namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Http\Request;
use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;

class Authenticate
{
    /**
     * Handle an incoming request
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->header('Authorization')) {
            abort(401, 'No token present');
        }

        $token = JWTAuth::setRequest($request)->parseToken();

        if (!$token->check()) {
            abort(401, 'Token mismatch');
        }

        app()->instance('App\Models\User', $this->user($token->getClaim('sub')));

        return $next($request);
    }

    /**
     * Find the user by an id
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    protected function user($id)
    {
        return User::query()->findOrFail($id);
    }
}
