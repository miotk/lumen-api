<?php

/**
 *--------------------------------------------------------------------------
 * Application Routes
 *--------------------------------------------------------------------------
 * Here is where you can register all of the routes for an application.
 * It is a breeze. Simply tell Lumen the URIs it should respond to
 * and give it the Closure to call when that URI is requested.
 *
 * @var $router \Laravel\Lumen\Routing\Router
 */

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/authenticate', 'AuthenticationController@login');
$router->post('/authenticate/logout', 'AuthenticationController@logout');
$router->post('/authenticate/register', 'AuthenticationController@register');

$router->get('posts/{slug}', 'PostController@find');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('posts', 'PostController@create');
    $router->patch('posts/{id}', 'PostController@update');
    $router->delete('posts/{id}', 'PostController@delete');
});
